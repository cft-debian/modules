# Class: fam
#
# This module manages fam
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class mdupont-fam {
      package { mdupont-fam: ensure => latest }

    file { '/usr/lib/libfam.la':
      type => 'file',
      source => "puppet:///modules/mdupont-fam/usr/lib/libfam.la",	
      replace => 'true',
      loglevel => 'notice',
      group => 'root',
      seluser => '',
      links => 'manage',
      checksum => 'md5',
      provider => 'posix',
      backup => 'puppet',
      selrole => '',
      ensure => 'file',
      seltype => '',
      owner => 'root',
      mode => '0755',
      selrange => '',
      purge => 'false'
    }

    file { '/usr/sbin/famd':
      type => 'file',
      source => "puppet:///modules/mdupont-fam/usr/sbin/famd",	
      replace => 'true',
      loglevel => 'notice',
      group => 'root',
      seluser => '',
      links => 'manage',
      checksum => 'md5',
      provider => 'posix',
      backup => 'puppet',
      selrole => '',
      ensure => 'file',
      seltype => '',
      owner => 'root',
      mode => '0755',
      selrange => '',
      purge => 'false'
    }

    file { '/usr/lib/libfam.so.0.0.0':
      type => 'file',
      sourceselect => 'first',
      source => "puppet:///modules/mdupont-fam/usr/lib/libfam.so.0.0.0",	
      replace => 'true',
      loglevel => 'notice',
      group => 'root',
      seluser => '',
      links => 'manage',
      checksum => 'md5',
      provider => 'posix',
      backup => 'puppet',
      selrole => '',
      ensure => 'file',
      seltype => '',
      owner => 'root',
      mode => '0755',
      selrange => '',
      purge => 'false'
    }

    file { '/usr/include/fam.h':
        source => "puppet:///modules/mdupont-fam/usr/include/fam.h",
      
        sourceselect => 'first',
        replace => 'true',
        loglevel => 'notice',
        links => 'manage',
        checksum => 'md5',
        provider => 'posix',
        backup => 'puppet',
        ensure => 'file',
        purge => 'false'
    }


    file { '/usr/lib/libfam.a':
      source => "puppet:///modules/mdupont-fam/usr/lib/libfam.a",
      type => 'file',
      sourceselect => 'first',
      replace => 'true',
      loglevel => 'notice',
        group => 'root',
        seluser => '',
        links => 'manage',
        checksum => 'md5',
        provider => 'posix',
        backup => 'puppet',
        selrole => '',
        ensure => 'file',
        seltype => '',
        owner => 'root',
        mode => '0644',
        selrange => '',
        purge => 'false'
    }


    file { '/usr/lib/libfam.so.0':
      type => 'link',
      sourceselect => 'first',
      target => '/usr/lib/libfam.so.0.0.0',        
      replace => 'true',
      loglevel => 'notice',
      group => 'root',
      seluser => '',
      links => 'manage',
      checksum => 'md5',
      provider => 'posix',
        backup => 'puppet',
      selrole => '',
      ensure => 'link',
      seltype => '',
      owner => 'root',
      mode => '0777',
      selrange => '',
      purge => 'false'
      
    }
    
    file { '/usr/lib/libfam.so':
      type => 'link',
      target => '/usr/lib/libfam.so.0.0.0',        
      replace => 'true',
      loglevel => 'notice',
      group => 'root',
      seluser => '',
      links => 'manage',
      checksum => 'md5',
      provider => 'posix',
      backup => 'puppet',
      selrole => '',
      ensure => 'link',
      seltype => '',
      owner => 'root',
      mode => '0777',
      selrange => '',
      purge => 'false'
    }

}
